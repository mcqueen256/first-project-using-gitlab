FROM rust:latest
WORKDIR /usr/src/first-project-using-gitlab
COPY . .
RUN pwd
RUN ls /usr/src/first-project-using-gitlab
RUN cargo build --release
RUN cargo install
CMD ["first-project-using-gitlab"]
