//             .^. .  _
//            /: ||`\/ \~  ,
//           , [   &    / \ y'
//          {v':   `\   / `&~-,
//         'y. '    |`   .  ' /
//          \   '  .       , y
//          v .        '     v
//          V  .~.      .~.  V
//          : (  0)    (  0) :      This is only a test project.
//           i `'`      `'` j           Only shows that I can use gitlab ;P
//           i     __    ,j
//            `%`~....~'&
//          <~o' /  \/` \-s,
//           o.~'.  )(  r  .o ,.
//          o',  %``\/``& : 'bF
//         d', ,ri.~~-~.ri , +h
//         `oso' d`~..~`b 'sos`
//             d`+ II +`b
//             i_:_yi_;_y

fn addition(a: i32, b: i32) -> i32 {
    a + b
}

fn main() {
    println!("The first commit!, {}", addition(7, 1));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_addition() {
        assert_eq!(addition(3, 7), 10);
    }
}
